#include "stdafx.h"
#include "Pin.h"

//Constructors
Pin::Pin(Drive pType, logic_pin pPin, direction_pin pDirection) :
	mType(pType), mPin(pPin), mDirection(pDirection)
{
	switch (mType)
	{
	case Pin::Drive::Input:
		mDirection = Direction::Input;
		break;
	case Pin::Drive::PushPull:
		mDirection = Direction::Output;
		break;
	case Pin::Drive::OpenDrain:
		mPin = Logic::Low;
		break;
	case Pin::Drive::OpenSource:
		mPin = Logic::High;
		break;
	}
}
Pin::Pin(logic_pin pPin, direction_pin pDirection) :
	mType(Drive::Input), mPin(pPin), mDirection(pDirection)
{
	mDirection = Direction::Input;
}

//Allows changing of type after construction
void Pin::SetType(Drive pType)
{
	mType = pType;
	switch (mType)
	{
	case Pin::Drive::Input:
		mDirection = Direction::Input;
		break;
	case Pin::Drive::PushPull:
		mDirection = Direction::Output;
		break;
	case Pin::Drive::OpenDrain:
		mPin = Logic::Low;
		break;
	case Pin::Drive::OpenSource:
		mPin = Logic::High;
		break;
	}
}

//Operators that allow easy interface
void Pin::operator=(Logic const pInput)
{
	switch (mType)
	{
	case Pin::Drive::PushPull:
		mPin = pInput;
		break;
	case Pin::Drive::OpenDrain:
		mDirection = (Direction)pInput;
		break;
	case Pin::Drive::OpenSource:
		mDirection = (Direction)(pInput ^ 1); //Invert first bit (true or false)
		break;
	}
}
Pin::Logic Pin::operator()()
{
	return mPin();
}
Pin::operator Logic()
{
	return mPin();
}