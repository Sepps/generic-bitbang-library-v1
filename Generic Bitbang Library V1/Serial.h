#ifndef SERIAL_H
#define SERIAL_H

#include "Pin.h"
#include "GetSet.h"

struct Serial
{
	using Drive = Pin::Drive;
	using Logic = Pin::Logic;
	using byte = unsigned char;

	//All protocol definitions must contain read and write functions
	virtual byte const	Read()				= 0;
	virtual void		Write(byte pInput)	= 0;
};

#endif