#ifndef SPI_H
#define SPI_H
#include "Serial.h"

class SPI : public Serial
{
public:
	enum class Mode
	{
		Mode0 = 0b00,	//	
		Mode1 = 0b01,	//	
		Mode2 = 0b10,	//	
		Mode3 = 0b11	//
	};

	SPI(Mode pMode, Pin const pCLK, Pin const pCS, Pin const pMISO, Pin const pMOSI);
	inline bool const phase() const;
	inline Logic const polarity() const;

	Serial::byte const Read();
	void Write(Serial::byte pInput);
private:
	bool mInvertClock;

	//Stores the read values
	Serial::byte mBuffer;

	//Standard SPI mode
	Mode const mMode;

	//Standard SPI pins
	Pin mCS, mCLK, mMOSI, mMISO;

	//General functions
	inline void Select();
	inline void Deselect();
};


#endif