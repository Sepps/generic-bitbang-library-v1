#include "stdafx.h"
#include "SPI.h"

SPI::SPI(Mode pMode, Pin const pCLK, Pin const pCS, Pin const pMISO, Pin const pMOSI) :
	mBuffer(0),
	mMode(pMode),
	mCS(pCS),
	mCLK(pCLK),
	mMISO(pMISO),
	mMOSI(pMOSI)
{
	//Set pin directions
	mCS.SetType(Pin::Drive::PushPull);
	mCLK.SetType(Pin::Drive::PushPull);
	mMOSI.SetType(Pin::Drive::PushPull);
	mMISO.SetType(Pin::Drive::Input);
	
	//Set chip select to deselect
	mCS = Logic::High;
	switch (mMode)
	{
	case Mode::Mode0:
		mCLK = Logic::Low;
		break;
	case Mode::Mode1:
		mCLK = Logic::Low;
		break;
	case Mode::Mode2:
		mCLK = Logic::High;
		break;
	case Mode::Mode3:
		mCLK = Logic::High;
		break;
	}
	mInvertClock = phase();
}
inline bool const SPI::phase() const
{
	return (static_cast<byte>(mMode) & 0b01) > 0;
}
inline SPI::Logic const SPI::polarity() const
{
	return (Logic)((static_cast<byte>(mMode) & 0b10) > 0);
}

Serial::byte const SPI::Read()
{
	return mBuffer;
}
void SPI::Write(Serial::byte pInput)
{
	Select();
	for (auto i = 0; i < 8; ++i)
	{
		//Output on first edge
		mMOSI = (Logic)(pInput >= 0x80);
		mCLK = (Logic)mInvertClock;

		//Input on second edge, Shift in a bit
		mBuffer >>= 1;
		mBuffer |= mMISO << 7;
		mCLK = (Logic)(!(bool)mCLK);

		//Move to next bit for output
		pInput <<= 1;
	}
	mCLK = polarity();
	Deselect();
}

//General functions
inline void SPI::Select()
{
	mCS = Logic::Low;
}
inline void SPI::Deselect()
{
	mCS = Logic::High;
}